import requests
import random


def emulate(quiz_result_id=1):
    honesty = random.choice(range(60, 100)) - (
        random.choice(range(100000, 500000)) / 100000
    )
    data = {"honesty": honesty}

    requests.post(
        f"https://serhii.univproject.tk/app/quiz/post_honesty/{quiz_result_id}/",
        data=data,
    )


while True:
    for i in range(100):
        emulate(quiz_result_id=i)
