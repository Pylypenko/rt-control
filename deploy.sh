docker stop serhii-apz-backend || echo "Container was not run"
docker container rm serhii-apz-backend || echo "Container was not present"
docker image rm serhii-apz-backend:latest || echo "Image was not found"
docker load < serhii-apz-backend.tar
docker run -d --network host --name serhii-apz-backend serhii-apz-backend:latest
