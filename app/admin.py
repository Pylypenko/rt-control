from django.contrib import admin

from app.models import Answer, Question, Quiz, QuizResult, Report


class AnswerInline(admin.TabularInline):
    model = Answer


class QuestionAdmin(admin.ModelAdmin):
    inlines = [AnswerInline]
    filter_horizontal = ("quizes",)
    search_fields = ["pk", "answers_type", "text"]


class QuizAdmin(admin.ModelAdmin):
    filter_horizontal = ("allowed_groups", "passed_users")
    search_fields = ["title", "start_time", "end_time", "pk"]


class QuizResultAdmin(admin.ModelAdmin):
    readonly_fields = ("quiz", "user", "total_result", "questions_snapshot")
    search_fields = ["user__email", "pk"]


class ReportAdmin(admin.ModelAdmin):
    readonly_fields = ("user", "quiz_result", "reason")
    search_fields = ["user__email", "state", "reason", "pk"]


class AnswerAdmin(admin.ModelAdmin):
    pass


admin.site.register(Question, QuestionAdmin)
admin.site.register(Quiz, QuizAdmin)
admin.site.register(QuizResult, QuizResultAdmin)
admin.site.register(Report, ReportAdmin)
admin.site.register(Answer, AnswerAdmin)
