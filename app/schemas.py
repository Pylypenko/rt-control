from app.serializers.quiz import (
    QuizReadonlySwaggerListSerializer,
    StartQuizSwaggerSerializer,
)
from app.serializers.quiz_result import (
    QuizResultListReadonlySwaggerSerializer,
    QuizResultReadonlySwaggerSerializer,
    QuizResultSwaggerSerializer,
)
from app.serializers.report import (
    ReportListReadonlySwaggerSerializer,
    ReportReadonlySwaggerSerializer,
    ReportSwaggerSerializer,
)
from helpers.schemas import Schema

quizes_schema = Schema(response_200=QuizReadonlySwaggerListSerializer)
start_quiz_schema = Schema(response_200=StartQuizSwaggerSerializer)
submit_quiz_schema = Schema(
    response_201=QuizResultReadonlySwaggerSerializer,
    request_body=QuizResultSwaggerSerializer,
)
taken_quizes_schema = Schema(
    response_200=QuizResultListReadonlySwaggerSerializer
)
report_quiz_schema = Schema(
    response_201=ReportReadonlySwaggerSerializer,
    request_body=ReportSwaggerSerializer,
)
submitted_reports_schema = Schema(
    response_200=ReportListReadonlySwaggerSerializer
)
