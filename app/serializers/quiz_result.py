from django.utils import timezone
from rest_framework import serializers

from app.models import Answer, QuizResult, QuizSession
from app.serializers.question import (
    QuestionSnapshotSerializer,
    QuestionSnapshotSwaggerSerializer,
)
from app.serializers.quiz import (
    QuizReadonlySerializer,
    QuizReadonlySwaggerSerializer,
)


class QuizResultSerializer(serializers.ModelSerializer):
    quiz_token = serializers.CharField()
    questions_snapshot = QuestionSnapshotSerializer(many=True)

    def create(self, validated_data):
        quiz_token = validated_data.pop("quiz_token")
        quiz, session = self._get_quiz_and_session(quiz_token)

        validated_data["quiz"] = quiz
        validated_data["user"] = self.context["user"]
        validated_data["total_result"] = self._calculate_total_result(
            validated_data["questions_snapshot"]
        )
        self._mark_answers(validated_data["questions_snapshot"])

        instance = super().create(validated_data)

        self._record_end_of_quiz(instance, session)

        return instance

    def _get_quiz_and_session(self, token):
        quiz_session = QuizSession.objects.get(token=token)
        return quiz_session.quiz, quiz_session

    def _calculate_total_result(self, questions_snapshot):
        answers = [
            x["id"]
            for question in questions_snapshot
            for x in question["answers"]
            if x["is_ticked"]
        ]
        answers_points = Answer.objects.filter(
            pk__in=answers, is_correct=True
        ).values_list("points", flat=True)
        return sum(answers_points)

    def _mark_answers(self, questions_snapshot):
        answer_objects = [
            x for question in questions_snapshot for x in question["answers"]
        ]
        answers = Answer.objects.filter(
            pk__in=[x["id"] for x in answer_objects]
        )
        pk_to_answer = {str(x.pk): x for x in answers}
        for question in questions_snapshot:
            for answer_obj in question["answers"]:
                answer = pk_to_answer[answer_obj["id"]]
                answer_obj["is_correct"] = answer.is_correct
                answer_obj["points"] = answer.points

    def _record_end_of_quiz(self, instance, session):
        session.end_time = timezone.now()
        session.quiz_result = instance
        session.token = None
        session.save()

    class Meta:
        model = QuizResult
        fields = ("quiz_token", "questions_snapshot")


class QuizResultSwaggerSerializer(serializers.Serializer):
    quiz_token = serializers.CharField()
    questions_snapshot = QuestionSnapshotSerializer(many=True)


class QuizResultReadonlySerializer(serializers.ModelSerializer):
    questions_snapshot = QuestionSnapshotSerializer(many=True)
    total_result = serializers.FloatField()
    quiz = QuizReadonlySerializer()
    honesty = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )

    class Meta:
        model = QuizResult
        fields = ("questions_snapshot", "total_result", "quiz", "honesty")


class QuizResultReadonlySwaggerSerializer(serializers.Serializer):
    questions_snapshot = QuestionSnapshotSwaggerSerializer(many=True)
    total_result = serializers.FloatField()
    quiz = QuizReadonlySwaggerSerializer()
    honesty = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )


class QuizResultListReadonlySwaggerSerializer(serializers.Serializer):
    items = QuizResultReadonlySwaggerSerializer(many=True)
