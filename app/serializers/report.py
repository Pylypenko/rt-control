from rest_framework import serializers

from app.models import Report
from app.serializers.quiz_result import (
    QuizResultReadonlySerializer,
    QuizResultReadonlySwaggerSerializer,
)


class ReportSerializer(serializers.ModelSerializer):
    reason = serializers.CharField()

    def create(self, validated_data):
        validated_data["quiz_result"] = self.context["quiz_result"]
        validated_data["user"] = self.context["user"]
        return super().create(validated_data)

    class Meta:
        model = Report
        fields = ("reason",)


class ReportSwaggerSerializer(serializers.Serializer):
    reason = serializers.CharField()


class ReportReadonlySerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    quiz_result = QuizResultReadonlySerializer()
    reason = serializers.CharField()
    state = serializers.ChoiceField(choices=Report.STATES)

    class Meta:
        model = Report
        fields = ("id", "quiz_result", "reason", "state")


class ReportReadonlySwaggerSerializer(serializers.Serializer):
    id = serializers.CharField()
    quiz_result = QuizResultReadonlySwaggerSerializer()
    reason = serializers.CharField()
    state = serializers.ChoiceField(choices=Report.STATES)


class ReportListReadonlySwaggerSerializer(serializers.Serializer):
    items = ReportReadonlySwaggerSerializer(many=True)
