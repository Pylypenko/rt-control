import hashlib

from django.utils import timezone
from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers

from app.models import Quiz, QuizSession
from app.serializers.question import (
    QuestionReadonlySerializer,
    QuestionReadonlySwaggerSerializer,
)


class QuizReadonlySerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    title = serializers.CharField()
    start_time = serializers.DateTimeField()
    end_time = serializers.DateTimeField()

    class Meta:
        model = Quiz
        fields = ("id", "title", "start_time", "end_time")


class QuizReadonlySwaggerSerializer(serializers.Serializer):
    id = serializers.CharField()
    title = serializers.CharField()
    start_time = serializers.DateTimeField()
    end_time = serializers.DateTimeField()


class QuizReadonlySwaggerListSerializer(serializers.Serializer):
    items = QuizReadonlySwaggerSerializer(many=True)


class StartQuizSerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    title = serializers.CharField()
    start_time = serializers.DateTimeField()
    end_time = serializers.DateTimeField()
    questions = QuestionReadonlySerializer(many=True)
    quiz_token = serializers.SerializerMethodField()

    @swagger_serializer_method(serializers.CharField())
    def get_quiz_token(self, instance):
        user = self.context["user"]
        data_to_hash = user.email + str(timezone.now())
        token = hashlib.md5(data_to_hash.encode()).hexdigest()
        QuizSession.objects.create(quiz=instance, user=user, token=token)
        instance.passed_users.add(user)
        instance.save()
        return token

    class Meta:
        model = Quiz
        fields = (
            "id",
            "title",
            "start_time",
            "end_time",
            "questions",
            "quiz_token",
        )


class StartQuizSwaggerSerializer(serializers.Serializer):
    id = serializers.CharField()
    title = serializers.CharField()
    start_time = serializers.DateTimeField()
    end_time = serializers.DateTimeField()
    questions = QuestionReadonlySwaggerSerializer(many=True)
    quiz_token = serializers.CharField()
