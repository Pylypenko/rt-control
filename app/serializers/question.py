from rest_framework import serializers

from app.models import Question
from app.serializers.answer import (
    AnswerReadonlySerializer,
    AnswerReadonlySwaggerSerializer,
    AnswerSnapshotSerializer,
    AnswerSnapshotSwaggerSerializer,
)


class QuestionReadonlySerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    answers_type = serializers.ChoiceField(Question.ANSWER_TYPES)
    text = serializers.CharField()
    answers = AnswerReadonlySerializer(many=True)

    class Meta:
        model = Question
        fields = ("id", "answers_type", "text", "answers")


class QuestionReadonlySwaggerSerializer(serializers.Serializer):
    id = serializers.CharField()
    answers_type = serializers.ChoiceField(Question.ANSWER_TYPES)
    text = serializers.CharField()
    answers = AnswerReadonlySwaggerSerializer(many=True)


class QuestionSnapshotSerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    answers_type = serializers.ChoiceField(Question.ANSWER_TYPES)
    text = serializers.CharField()
    answers = AnswerSnapshotSerializer(many=True)

    class Meta:
        model = Question
        fields = ("id", "answers_type", "text", "answers")


class QuestionSnapshotSwaggerSerializer(serializers.Serializer):
    id = serializers.CharField()
    answers_type = serializers.ChoiceField(Question.ANSWER_TYPES)
    text = serializers.CharField()
    answers = AnswerSnapshotSwaggerSerializer(many=True)
