from rest_framework import serializers

from app.models import Answer


class AnswerReadonlySerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    text = serializers.CharField()

    class Meta:
        model = Answer
        fields = ("id", "text")


class AnswerReadonlySwaggerSerializer(serializers.Serializer):
    id = serializers.CharField()
    text = serializers.CharField()


class AnswerSnapshotSerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    text = serializers.CharField()
    is_ticked = serializers.BooleanField()
    is_correct = serializers.BooleanField(read_only=True)
    points = serializers.FloatField(read_only=True)

    class Meta:
        model = Answer
        fields = ("id", "text", "is_ticked", "is_correct", "points")


class AnswerSnapshotSwaggerSerializer(serializers.Serializer):
    id = serializers.CharField()
    text = serializers.CharField()
    is_ticked = serializers.BooleanField()
    is_correct = serializers.BooleanField(read_only=True)
    points = serializers.FloatField(read_only=True)
