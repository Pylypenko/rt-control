from rest_framework.exceptions import APIException


class QuizNotAllowedError(APIException):
    default_code = "quiz_not_allowed"
    default_detail = "You're not allowed to take this quiz"
    status_code = 403
