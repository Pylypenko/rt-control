from django.contrib.auth.models import Group, User
from django.contrib.postgres.fields import ArrayField, JSONField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone

from app.managers import QuizManager
from helpers.models import AbstractTimeStamp

TEN_SCORE_VALIDATORS = [MinValueValidator(0), MaxValueValidator(10)]


class Quiz(models.Model):
    title = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True)
    allowed_groups = models.ManyToManyField(Group, related_name="+")
    passed_users = models.ManyToManyField(User, related_name="quizes")

    objects = QuizManager()

    def is_accessible(self, user):
        now = timezone.now()
        is_time_correct = (now >= self.start_time) & (now <= self.end_time)
        is_first_try = user not in self.passed_users.all()
        result = is_first_try and is_time_correct
        return result

    def __str__(self):
        return f"Quiz - {self.pk}: {self.start_time} - {self.end_time}"


class Question(models.Model):
    ANSWER_TYPES = [("checkbox", "checkbox"), ("radio", "radio")]
    answers_type = models.CharField(
        max_length=20, null=True, blank=True, choices=ANSWER_TYPES
    )
    text = models.TextField(null=True, blank=True)
    quizes = models.ManyToManyField(Quiz, related_name="questions")

    def __str__(self):
        return f"Question - {self.pk}"


class Answer(models.Model):
    is_correct = models.BooleanField(null=True)
    points = models.IntegerField(null=True, validators=TEN_SCORE_VALIDATORS)
    text = models.TextField(null=True, blank=True)
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name="answers"
    )

    def __str__(self):
        return f"Answer - {self.pk}"


class QuizResult(models.Model):
    quiz = models.ForeignKey(
        Quiz, on_delete=models.CASCADE, null=True, related_name="results"
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="quiz_results"
    )
    total_result = models.FloatField(null=True)
    questions_snapshot = ArrayField(JSONField())
    honesty = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f"Quiz result - {self.pk}: {self.user.email}"


class QuizSession(models.Model):
    quiz = models.ForeignKey(
        Quiz, on_delete=models.CASCADE, null=True, related_name="sessions"
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="quiz_sessions"
    )
    quiz_result = models.ForeignKey(
        QuizResult,
        on_delete=models.CASCADE,
        null=True,
        related_name="sessions",
    )
    token = models.CharField(max_length=256, null=True, blank=True)
    start_time = models.DateTimeField(auto_now_add=True, null=True)
    end_time = models.DateTimeField(null=True)

    def __str__(self):
        return f"Quiz session - {self.pk}: {self.user.email}"


class Report(AbstractTimeStamp):
    STATES = [(x, x) for x in ["submitted", "approved", "rejected"]]

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="reports"
    )
    quiz_result = models.ForeignKey(
        QuizResult, on_delete=models.CASCADE, null=True, related_name="reports"
    )
    reason = models.TextField()
    state = models.CharField(
        choices=STATES, default="submitted", max_length=100
    )

    def __str__(self):
        return f"Report - {self.pk}: {self.user.email}"
