from django.urls import include, path
from rest_framework.routers import DefaultRouter

from app.views import QuizViewSet

router = DefaultRouter()
router.register(r"quiz", QuizViewSet, basename="quiz")

urlpatterns = [path("", include(router.urls))]
