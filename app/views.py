from django.db import transaction
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from app.exceptions import QuizNotAllowedError
from app.models import Quiz, Report, QuizResult
from app.schemas import (
    quizes_schema,
    report_quiz_schema,
    start_quiz_schema,
    submit_quiz_schema,
    taken_quizes_schema,
    submitted_reports_schema,
)
from app.serializers.quiz import QuizReadonlySerializer, StartQuizSerializer
from app.serializers.quiz_result import (
    QuizResultReadonlySerializer,
    QuizResultSerializer,
)
from app.serializers.report import ReportReadonlySerializer, ReportSerializer


class QuizViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]
    model_class = Quiz
    serializers = {
        "submit_quiz": QuizResultSerializer,
        "submit_report": ReportSerializer,
    }
    readonly_serializers = {
        "available_quizes": QuizReadonlySerializer,
        "taken_quizes": QuizResultReadonlySerializer,
        "start_quiz": StartQuizSerializer,
        "submit_quiz": QuizResultReadonlySerializer,
        "submit_report": ReportReadonlySerializer,
        "submitted_reports": ReportReadonlySerializer,
    }

    def get_serializer_class(self):
        return self.serializers[self.action]

    def get_readonly_serializer_class(self):
        return self.readonly_serializers[self.action]

    @quizes_schema
    @action(methods=["GET"], detail=False)
    def available_quizes(self, request, *args, **kwargs):
        queryset = self.model_class.objects.accessible(request.user)
        serializer_cls = self.get_readonly_serializer_class()
        serializer = serializer_cls(queryset, many=True)
        data = {"items": serializer.data}
        return Response(data, status=status.HTTP_200_OK)

    @start_quiz_schema
    @action(methods=["GET"], detail=False, url_path=r"start_quiz/(?P<id>\w+)")
    @transaction.atomic
    def start_quiz(self, request, id=None, *args, **kwargs):
        quiz = self.model_class.objects.get(pk=id)
        if quiz.is_accessible(request.user):
            serializer_cls = self.get_readonly_serializer_class()
            serializer = serializer_cls(quiz, context={"user": request.user})
            return Response(serializer.data, status=status.HTTP_200_OK)
        raise QuizNotAllowedError

    @submit_quiz_schema
    @action(
        methods=["POST"], detail=False, url_path=r"submit_quiz/(?P<id>\w+)"
    )
    @transaction.atomic
    def submit_quiz(self, request, id=None, *args, **kwargs):
        serializer_cls = self.get_serializer_class()
        serializer = serializer_cls(
            data=request.data, context={"user": request.user}
        )
        serializer.is_valid(raise_exception=True)

        instance = serializer.save()

        serializer_cls = self.get_readonly_serializer_class()
        serializer = serializer_cls(instance)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @taken_quizes_schema
    @action(methods=["GET"], detail=False)
    def taken_quizes(self, request, *args, **kwargs):
        serializer_cls = self.get_readonly_serializer_class()
        serializer = serializer_cls(request.user.quiz_results.all(), many=True)
        data = {"items": serializer.data}
        return Response(data, status=status.HTTP_200_OK)

    @report_quiz_schema
    @action(
        methods=["POST"], detail=False, url_path=r"submit_report/(?P<id>\w+)"
    )
    @transaction.atomic
    def submit_report(self, request, id=None, *args, **kwargs):
        quiz = self.model_class.objects.get(pk=id)
        quiz_result = quiz.results.get(user=request.user)

        serializer_cls = self.get_serializer_class()
        serializer = serializer_cls(
            data=request.data,
            context={"quiz_result": quiz_result, "user": request.user},
        )
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()

        serializer_cls = self.get_readonly_serializer_class()
        serializer = serializer_cls(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @submitted_reports_schema
    @action(methods=["GET"], detail=False)
    def submitted_reports(self, request, *args, **kwargs):
        reports = Report.objects.filter(user=request.user).order_by("-created")
        serializer_cls = self.get_readonly_serializer_class()
        serializer = serializer_cls(reports, many=True)
        data = {"items": serializer.data}
        return Response(data, status=status.HTTP_200_OK)

    @action(
        methods=["POST"],
        detail=False,
        url_path=r"post_honesty/(?P<id>\w+)",
        permission_classes=(),
        authentication_classes=(),
    )
    def post_honesty(self, request, id=None, *args, **kwargs):
        quiz_result = QuizResult.objects.get(pk=id)
        quiz_result.honesty = request.data["honesty"]
        quiz_result.save()
