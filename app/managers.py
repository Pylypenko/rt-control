from django.db.models import Manager


class QuizManager(Manager):
    def accessible(self, user):
        queryset = self.get_queryset()
        return [x for x in queryset if x.is_accessible(user)]
